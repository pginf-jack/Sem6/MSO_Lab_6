package pl.edu.pg.mso_lab_6

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import java.net.ServerSocket
import java.util.concurrent.atomic.AtomicReference


class MainActivity : AppCompatActivity(), SensorEventListener {
    private lateinit var sensorManager: SensorManager
    private var mLight: Sensor? = null
    private var currentValue: AtomicReference<String> = AtomicReference("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mLight = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        val port = 43560
        val thread = Thread {
            try {
                val ss = ServerSocket(port)
                Log.i("LocalPort", "" + ss.getLocalPort())
                Log.i("HostAddress", ss.getInetAddress().getHostAddress())
                Log.i("SocketAddress", ss.getLocalSocketAddress().toString())
                while (true) {
                    val socket = ss.accept()
                    Log.i("ss.accept", "" + ss.getLocalPort())
                    val connectionThread = Thread {
                        val bufor = ByteArray(5)
                        try {
                            while (true) {
                                val dlugosc = socket.getInputStream().read(bufor, 0, 5)
                                val s: String = String(bufor, 0, dlugosc)
                                if (s == "GIMME") {
                                    val output = currentValue.get()
                                    socket.getOutputStream().write(output.toByteArray())
                                    Log.i("ss.write", output)
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    connectionThread.start()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        thread.start()
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        // we don't care about changes in accuracy but we have to implement this method
    }

    override fun onSensorChanged(event: SensorEvent) {
        currentValue.set("${event.values[0]} ${event.values[1]} ${event.values[2]}")
    }

    override fun onResume() {
        super.onResume()
        mLight?.also { light ->
            sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }
}