# Echo client program
import socket
import sys
import time

HOST = sys.argv[1]
PORT = 12345
FREQUENCY = int(sys.argv[2])
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    while True:
        print("Sending message 'GIMME'")
        s.sendall(b"GIMME")
        data = s.recv(1024)
        print('Received message:', data.decode())
        time.sleep(FREQUENCY)
